const gulp = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminZopfli = require('imagemin-zopfli');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminGiflossy = require('imagemin-giflossy');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');

gulp.task('javascript',function(){
    gulp.src('./private/js/*.js')
        .pipe(babel())
		.pipe(uglify())
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('sass', function () {
    gulp.src('./private/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('imagemin', function(){
    gulp.src('./private/img/*.*')
        .pipe(imagemin([
            imageminPngquant({
                speed: 1,
                quality: 90 
            }),
            imageminZopfli({
                more: true
            }),
            imageminGiflossy({
                optimizationLevel: 3,
                optimize: 3,
                lossy: 2
            }),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            imagemin.jpegtran({
                progressive: true
            }),
            imageminMozjpeg({
                quality: 90
            })
        ]))
        .pipe(gulp.dest('./public/img/'));
});

gulp.task('watch', function(){
    gulp.watch('./private/js/*.js', ['javascript']);
    gulp.watch('./private/sass/*.scss', ['sass']);
});

gulp.task('default', ['javascript','sass','watch']);