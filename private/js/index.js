class Portfolio {
	constructor() {
		app.DEBUG = true;
		app.log('KOUTARO TSURUTA PORTFOLIO');
		
		document.getElementById('loading-wrap').style.display = 'none';
		document.getElementById('skip').style.display = 'block';
		document.getElementById('wrap-inline').style.right = document.getElementById('wrap-inline').clientWidth - document.getElementById('wrap-inline').offsetWidth + 'px';
		
		this.CANVAS_WIDTH = window.innerWidth;
		this.CANVAS_HEIGHT = window.innerHeight;

		const all_canvas = document.getElementsByTagName('canvas');

		const setSize = (e) => {
			e.style.width = this.CANVAS_WIDTH + 'px';
			e.style.height = this.CANVAS_HEIGHT + 'px';
			e.width = this.CANVAS_WIDTH;
			e.height = this.CANVAS_HEIGHT;
		}

		this.canvas_gl = document.querySelector('#portfolio-gl');
		setSize(this.canvas_gl);
		this.canvas_2d = document.querySelector('#portfolio-2d');
		setSize(this.canvas_2d);
		this.stage_gl = new createjs.SpriteStage(this.canvas_gl);
		this.stage_2d = new createjs.Stage(this.canvas_2d);
		
		this.container = new createjs.Container();
		this.container.setBounds(0, 0, window.innerWidth,window.innerHeight);
		this.container.x = 0;
		this.container.y = 0;
		this.stage_2d.addChild(this.container);
		
		createjs.Ticker.addEventListener('tick', this.stage_gl);
		createjs.Ticker.addEventListener('tick', this.stage_2d);

		createjs.Ticker.setFPS(30);

		this.queue = new createjs.LoadQueue();
		this.queue.loadManifest([
			{id: 'particle', src: 'https://s3-ap-northeast-1.amazonaws.com/audiopens/portfolio_particles.png'},
			{id: 'name', src: 'https://s3-ap-northeast-1.amazonaws.com/audiopens/portfolio_name.png'},
			{id: 'my', src: 'https://s3-ap-northeast-1.amazonaws.com/audiopens/portfolio_my.png'}
			// {id: 'particle', src: 'public/img/particle.png'},
			// {id: 'name', src: 'public/img/myName.png'},
			// {id: 'my', src: 'public/img/myPortfolio.png'}
		]);
		this.queue.on('complete', this.fs, this);
		
		window.addEventListener('resize',function(){
			matterCanvas.width = window.innerWidth;
			matterCanvas.height = window.innerHeight;
		});
		

	}
	fs() {
		
		const this_ = this;
		let hidden  = false;

		const wrap = document.getElementById('wrap');
		const video = document.querySelector('#video');

		const load_canvas = document.createElement('canvas');
		const ctx = load_canvas.getContext('2d');
		load_canvas.width = window.innerWidth;
		load_canvas.height = window.innerHeight;
		const load_stage = new createjs.Stage(load_canvas);

		const image_name = this.queue.getResult('name');
		const name = new createjs.Bitmap(image_name);
		name.x = 0;
		name.y = 0;
		load_stage.addChild(name);
		load_stage.update();

		const NAME_TW = Math.ceil(name.getBounds().width);
		const NAME_TH = Math.ceil(name.getBounds().height);

		const NAME_X = this.canvas_2d.width / 2 - NAME_TW / 2;
		const NAME_Y = this.canvas_2d.height / 2 - NAME_TH / 2;

		const pixcelColors_ver1 = ctx.getImageData(0, 0, NAME_TW, NAME_TH).data;
		const nameAlpha = [];

		for(let w=0;w< NAME_TW;w++) {
			nameAlpha[w] = [];
			for(let h=0;h<NAME_TH;h++) {
				const checkAlpha = (pixcelColors_ver1[(w + h * NAME_TW) * 4 + 3] === 0);
				nameAlpha[w][h] = checkAlpha;
			}
		}

		const name_particles = [];
		const name_particle_x = [];
		const name_particle_y = [];
		for (let w = 0,cnt = 0; w < NAME_TW; w++) {
			for (let h = 0; h < NAME_TH; h++) {
				if (nameAlpha[w][h] === true) {
					continue;
				}
					
				name_particle_x[cnt] = w + NAME_X;
				name_particle_y[cnt] = h + NAME_Y;

				const image_particle = this_.queue.getResult('particle');
				const particles = new createjs.Bitmap(image_particle);

				particles.x = (Math.random()*this_.canvas_2d.width)-1;
				particles.y = this_.canvas_2d.height + 200;

				this_.stage_gl.addChild(particles);

				name_particles[cnt] = particles;

				cnt++;
			}
		}

		load_stage.removeAllChildren();

		const image_my = this.queue.getResult('my');
		const title = new createjs.Bitmap(image_my);
		title.x = 0;
		title.y = 0;

		load_stage.addChild(title);
		load_stage.update();

		const TITLE_TW = Math.ceil(title.getBounds().width);
		const TITLE_TH = Math.ceil(title.getBounds().height) + 50;

		const TITLE_X = this.canvas_2d.width / 2 - TITLE_TW / 2;
		const TITLE_Y = this.canvas_2d.height / 2 - TITLE_TH / 2;

		const pixcelColors_ver2 = ctx.getImageData(0, 0, TITLE_TW, TITLE_TH).data;
		const titleAlpha = [];

		for(let w = 0; w < TITLE_TW; w++) {
			titleAlpha[w] = [];
			for(let h = 0; h < TITLE_TH; h++) {
				const checkAlpha = (pixcelColors_ver2[(w + h * TITLE_TW) * 4 + 3] === 0);
				titleAlpha[w][h] = checkAlpha;
			}
		}

		const title_particle_x = [];
		const title_particle_y = [];
		for (let w = 0,cnt = 0; w < TITLE_TW; w++) {
			for (let h = 0; h < TITLE_TH; h++) {
				if (titleAlpha[w][h] === true) {
					continue;
				}
	
				title_particle_x[cnt] = w + TITLE_X;
				title_particle_y[cnt] = h + TITLE_Y;
				cnt++;
			}
		}
		
		load_stage.removeAllChildren();
		load_stage.update();

		for(let i=0;i<(name_particles.length-1);i++) {
			setTimeout(function(i){
				TweenMax.to(name_particles[i], 1, { x: name_particle_x[i], y: name_particle_y[i], onComplete:()=>{
					if(i === (name_particles.length-2)) {
						change_name();
					}
				}});
			}.bind(null, i + 1), 1 * i);
		}

		function change_name() {
			for(let i=0;i<(name_particles.length-1);i++) {
				setTimeout(function(i){
					const PX = (Math.random()*this_.canvas_2d.width)-1;
					const PY = (Math.random()*this_.canvas_2d.height)-1;
					TweenMax.to(name_particles[i], 0.5, { x: PX, y: PY, onComplete:()=>{
						if(i>(title_particle_x.length-1)) {
							this_.stage_gl.removeChild(name_particles[i]);
							if(i === (name_particles.length-2)) {
								title_move();
							}
						}
					}});
				}.bind(null, i + 1), 0.5 * i);
			}
		}

		function title_move() {
			name_particles.length = title_particle_x.length;
			for(let i=0;i<(name_particles.length-1);i++) {
				setTimeout(function(i){
					TweenMax.to(name_particles[i], 1, { x: title_particle_x[i], y: title_particle_y[i], onComplete:()=>{
						if(i === (name_particles.length-2)) {
							this_.stage_gl.removeAllChildren();
							TweenMax.to(document.getElementById('title-wrap'), 1, { opacity: 1});
							TweenMax.to(document.getElementById('title'), 2, { opacity: 1, onComplete:()=>{
								man_act();
							}});
						}
					}});
				}.bind(null, i + 1), 0.5 * i);
			}
		}

		const object = {
			images: ['https://s3-us-west-2.amazonaws.com/s.cdpn.io/244188/portfolio_man.png'],
			// images: ['public/img/portfolio_man.png'],
			frames: [
				[1, 1, 294, 521, 0, -157, -64],
				[1, 524, 289, 520, 0, -168, -66],
				[297, 1, 280, 515, 0, -171, -72],
				[1, 1046, 268, 516, 0, -183, -74],
				[579, 1, 273, 508, 0, -178, -71],
				[854, 1, 266, 513, 0, -185, -73],
				[1122, 1, 253, 514, 0, -198, -82],
				[1377, 1, 253, 505, 0, -198, -82],
				[1632, 1, 228, 529, 0, -178, -32],
				[271, 1046, 220, 529, 0, -213, -32],
				[292, 524, 219, 520, 0, -191, -64],
				[513, 518, 218, 530, 0, -191, -61],
				[493, 1050, 253, 505, 0, -198, -79],
				[733, 516, 218, 531, 0, -190, -56],
				[748, 1049, 217, 530, 0, -190, -56],
				[953, 517, 217, 526, 0, -191, -67],
				[967, 1045, 217, 531, 0, -190, -61],
				[1172, 517, 216, 526, 0, -191, -63],
				[1390, 508, 216, 536, 0, -190, -53],
				[1608, 532, 237, 504, 0, -169, -57],
				[1186, 1046, 221, 517, 0, -190, -69],
				[1608, 1038, 216, 537, 0, -190, -53]
			],
			animations: {
				'walk': {
					frames: [20,11,18,14,17,15,16,21,13,10],
					speed: 0.5
				},
				'rei': {
					frames: [9,8,19],
					speed: 0.3,
					next: false
				},
				'run': {
					frames: [1,3,6,5,0,2,7,12,4],
					speed: 0.5
				}
			}
		};

		const sprite = new createjs.SpriteSheet(object);
		const man = new createjs.Sprite(sprite);

		const man_act = () => {
			document.getElementById('skip').remove();
			
			man.scaleX = -0.5;
			man.scaleY = 0.5;
			man.x = this_.CANVAS_WIDTH + 300;
			man.y = (this_.CANVAS_HEIGHT - window.innerHeight) / 2 + (window.innerHeight - 300);
			man.gotoAndPlay('walk');
			this.container.addChild(man);

			const CENTER_PT = Math.round(this_.CANVAS_WIDTH / 2 + man.getBounds().width / 2);
			setTimeout(()=>{
				TweenMax.to(man, 4, { x: CENTER_PT, onComplete:()=>{
					man.gotoAndStop('walk');
					man.gotoAndPlay('rei');
					if(hidden != true) {
						wrap.classList.add('on');
						setTimeout(()=>{
							man.gotoAndStop('rei');
							video.play();
							man.gotoAndPlay('walk');
							TweenMax.to(man, 3, { x: -100, onComplete:()=>{
								app.log('Complete Animation');
								document.getElementById("portfolio-gl").remove();
								document.getElementById("portfolio-2d").remove();
								document.getElementsByTagName('html')[0].style.overflow = 'visible';
								document.getElementsByTagName('body')[0].style.overflowY = 'scroll';
								physicalWeb();
							}});
						},2000);
					}
				}});
			},1000);
		}
		
		document.getElementById('skip').addEventListener('click',()=>{
			app.log('Skip Animation');
			this.canvas_gl.remove();
			this.canvas_2d.remove();
			hidden = true;
			TweenMax.to(document.getElementById('title-wrap'), 0, { opacity: 1});
			TweenMax.to(document.getElementById('title'), 0, { opacity: 1});
			wrap.classList.add('on');
			video.play();
			document.getElementById('skip').remove();
			document.getElementsByTagName('html')[0].style.overflow = 'visible';
			document.getElementsByTagName('body')[0].style.overflowY = 'scroll';
			physicalWeb();
		},false);
		
		function physicalWeb () {
			const Engine = Matter.Engine,
				Events = Matter.Events,
				Render = Matter.Render,
				Runner = Matter.Runner,
				Composite = Matter.Composite,
				Composites = Matter.Composites,
				Common = Matter.Common,
				MouseConstraint = Matter.MouseConstraint,
				Mouse = Matter.Mouse,
				World = Matter.World,
				Vertices = Matter.Vertices,
				Svg = Matter.Svg,
				Bodies = Matter.Bodies;

			const engine = Engine.create(),
				world = engine.world;

			const matterCanvas = document.getElementById('matterCanvas');
			const render = Render.create({
				canvas : matterCanvas,
				engine: engine,
				options: {
					width: window.innerWidth,
					height: window.innerHeight,
					wireframes: false
				}
			});
			Render.run(render);

			const runner = Runner.create();
			Runner.run(runner, engine);

			const stack = Composites.stack(0, 0, 100, 1, 0, 0, function(x, y) {
				const random_x = getRandom(-window.innerWidth/2,window.innerWidth/2);
				const random_y = getRandom(0,1300);
				const random_circle = getRandom(0,30);
				return Bodies.circle(random_x, random_y, random_circle, { friction: 0.00001, restitution: 0.5 });
			});
			World.add(world, stack);
			World.add(world, [
				Bodies.rectangle(window.innerWidth/2-650, window.innerHeight-30, 750, 80, {
				isStatic: true,
				render: {
						strokeStyle: '#ffffff',
						sprite: {
								texture: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/244188/myportfolio2016ver.svg'
						}
				}})
			]);

			const mouse = Mouse.create(render.canvas),
				mouseConstraint = MouseConstraint.create(engine, {
					mouse: mouse,
					constraint: {
						stiffness: 0.2,
						render: {
							visible: false
						}
					}
				});

			World.add(world, mouseConstraint);
			render.mouse = mouse;

			Render.lookAt(render, {
				min: { x: 0, y: 0 },
				max: { x: 1, y: window.innerHeight }
			});

			function getRandom(min, max) {
				return Math.random() * (max - min) + min;
			}

			Engine.run(engine);
			Render.run(render);

			(function runAnimate () {
				const random_x = getRandom(-window.innerWidth/2,window.innerWidth/2);
				const random_circle = getRandom(0,30);
				World.add(world, [
					Bodies.circle(random_x, -100, random_circle, { friction: 0.00001, restitution: 0.5, density: 0.001 })
				]);
				window.requestAnimationFrame(runAnimate);

				function getRandom(min, max) {
				return Math.random() * (max - min) + min;
			}}());
		
		}
	}

};

window.addEventListener('load',function(){
	window.scroll(0,0);
	setTimeout(function(){
		new Portfolio();
	},2000);
});
